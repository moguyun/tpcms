<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/User'; //当前控制器)
ACTION='/dev/index.php/Admin/User/index';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
    <li class="current"><a href="javascript:;"><?php if($_GET["role"] == 2): ?>会员<?php else: ?>管理员<?php endif; ?>列表</a></li>
     
    </ul>
  </div>
  <div class="mb10">
  <a href="<?php echo U('User/add',array('role'=>$_GET['role'],'grade_gid'=>$_GET['grade_gid']));?>"  class="btn" title="新增会员"><span class="add"></span>新增<?php if($_GET["role"] == 2): ?>会员<?php else: ?>管理员<?php endif; ?></a>
  </div>
  <div class="h_a">搜索</div>
  <form method="get" action="<?php echo U('index');?>">
 
    <div class="search_type cc mb10">
      <div class="mb10"> 
        <span class="mr20">时间：
        <input type="text" name="start_time" class="input length_2 J_date" value="<?php echo ($_GET['start_time']); ?>" style="width:80px;" placeholder="选择时间">-<input type="text" class="input length_2 J_date" name="end_time" value="<?php echo ($_GET['end_time']); ?>" style="width:80px;" placeholder="选择时间">
        <select class="select_2" name="is_lock" style="width:100px;">
          <option value='3' <?php if(3 == $_GET["is_lock"]): ?>selected='selected'<?php endif; ?>>状态</option>
		      <option value='0' <?php if(0 == $_GET["is_lock"]): ?>selected='selected'<?php endif; ?>>正常</option>
          <option value='1' <?php if(1 == $_GET["is_lock"]): ?>selected='selected'<?php endif; ?>>锁定</option>
        </select>
        <?php if($_GET["role"] == 2): ?><select class="select_2" name="grade_gid" style="width:100px;">
            <option value='0' >会员等级</option>
            <?php if(is_array($grade)): foreach($grade as $key=>$v): ?><option value='<?php echo ($v["gid"]); ?>' <?php if($v["gid"] == $_GET["grade_gid"]): ?>selected='selected'<?php endif; ?> ><?php echo ($v["gname"]); ?></option><?php endforeach; endif; ?>
           
          </select>
        <?php else: ?>
           <select name="group_id" class="hd-w100">
            <option value="0">全部角色</option>
            <?php if(is_array($authGroup)): $i = 0; $__LIST__ = $authGroup;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>" <?php if($_GET["group_id"] == $v["id"]): ?>selected='selected'<?php endif; ?>><?php echo ($v["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
          </select><?php endif; ?>
        <select class="select_2" name="keytype" style="width:70px;">
          <option value='username'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "username"): ?>selected='selected'<?php endif; ?>>用户名</option>
          <option value='nickname'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "nickname"): ?>selected='selected'<?php endif; ?>>昵称</option>
          <option value='email'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "email"): ?>selected='selected'<?php endif; ?>>邮箱</option>
        </select>
        关键字：
        <input type="text" class="input length_2" name="keyword" style="width:200px;" value="<?php echo ($_GET['keyword']); ?>" placeholder="请输入关键字...">
          
          <input type="hidden" value="<?php echo ($_GET['role']); ?>" name="role">
          <button class="btn" type="submit">搜索</button>
        </span>
      </div>
    </div>
  </form>
  <form class="J_ajaxForm" action="" method="post">
    <div class="table_list">
      <table width="100%">
	        <colgroup>
	        <col width="16">
	        <col width="50">
	        <col width="">
	        <col width="100">
          <?php if($_GET["role"] == 2): ?><col width="150"><?php endif; ?>
	        <col width="150">

	        <col width="60">
	        <col width="150">
	        <col width="100">
	        </colgroup>
	        <thead>
	        <tr>
	            <td><label><input type="checkbox" class="J_check_all" data-direction="x" data-checklist="J_check_x"></label></td>
	          
	            <td align="left">ID</td>
	            <td>用户名</td>
              <td>昵称</td>
              <?php if($_GET["role"] == 2): ?><td>等级</td><?php endif; ?>
	            <td>邮箱</td>
	            <td>状态</td>
	            <td align="center"><span>注册时间</span></td>
	            <td align="center">管理操作</td>
	          </tr>
	        </thead>
          <?php if($data): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
	            <td>
	           	 	<input type="checkbox" class="J_check" data-yid="J_check_y" data-xid="J_check_x" name="uids[<?php echo ($value["uid"]); ?>]" value="<?php echo ($value["uid"]); ?>"  <?php if($value["uid"] == 1): ?>disabled='disabled'<?php endif; ?>>
	            </td>
	            <td align="left"><?php echo ($value["uid"]); ?></td>
	            <td><?php echo ($value["username"]); ?></td>
              <td><?php echo ($value["nickname"]); ?></td>
              <?php if($_GET["role"] == 2): ?><td><?php echo ($value["gname"]); ?></td><?php endif; ?>
	            <td><?php echo ($value["email"]); ?></td>

	            <td><?php if($value["is_lock"]): ?>已锁定<?php else: ?>正常<?php endif; ?></td>
	            <td align="center"><?php echo (format_date($value["addtime"])); ?></td>
	            <td align="center">
	            	<a href="<?php echo U('User/edit',array('uid'=>$value["uid"],'role'=>$value['role']));?>" >修改</a> 
	            	|
                <?php if($value["uid"] == 1): ?><font color="#BEBEBE"> 删除</font>
                <?php else: ?>
	            	  <a href="<?php echo U('User/del',array('uid'=>$value["uid"],'role'=>$value['role']));?>" class="J_ajax_del" >删除</a><?php endif; ?>
	            </td>
	        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        <?php else: ?>
        <tr><td colspan="9">没有找到符合条件的记录</td></tr><?php endif; ?>
	        
        </table>
      	<div class="p10"><div class="pages"> <?php echo ($page); ?> </div> </div>
     
    </div>
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <label class="mr20"><input type="checkbox" class="J_check_all" data-direction="y" data-checklist="J_check_y">全选</label>                
       
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('check');?>">锁定</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('cancel_check');?>">解锁</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('batch_delete');?>">删除</button>
      	
      </div>
    </div>
  </form>
</div>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.common.js"></script>

</body>
</html>