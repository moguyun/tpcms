// 定义是否是POST提交常量
$type = $_SERVER['REQUEST_METHOD']=='GET'? FALSE : TRUE;
define('IS_POST', $type);

//定义IS_AJAX
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
	define('IS_AJAX', TRUE);
}else
{
	define('IS_AJAX', FALSE);
}