<?php
/** [留言板模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 15:59:30
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class FeedbackLogic extends Model{

	/**
	 * [get_all 读取所有数据]
	 * @param  [type] $map         [description]
	 * @param  [type] $order       [description]
	 * @param  [type] $sort        [description]
	 * @param  [type] $currentPage [description]
	 * @param  [type] $listRows    [description]
	 * @return [type]              [description]
	 */
	public function get_all($map,$order,$sort,$currentPage,$listRows)
	{
		$data = $this->where($map)->order($order.' '.$sort)->page($currentPage.','.$listRows)->select();
		return $data;
	}

	/**
	 * [get_one 读取一个字段的信息]
	 * @return [type] [description]
	*/
	public function get_one($fid)
	{
		$this->save(array('fd_id'=>$fid,'lookstate'=>2));
		return $this->find($fid);
	}

	/**
	 * [del 删除]
	 * @param  [type] $fid [description]
	 * @return [type]      [description]
	 */
	public function del($fid)
	{
		$fids = explode(',',$fid);
		foreach($fids as $fid)
		{

			$this->delete($fid);
		}

		return true;
	}
	

}