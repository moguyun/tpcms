<?php
/** [广告管理]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 11:28:48
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 11:40:46
 */
namespace Admin\Logic;
use Think\Model;
use Think\Upload;
class AdLogic extends Model{


	// 自动验证
    // array(验证字段,验证规则,错误提示,[验证条件,附加规则,验证时间])
    protected $_validate=array(
    	//广告位置
    	array('position_psid','/^[^0]\d*$/','请选择广告位置',1,'regex',3),  
        // 广告名称验证
        array('name','require','广告名称必须填写',1),
     	array('sort','require','请输入排序值',1,'regex',3),
		array('sort','/^\d+$/i','排序值只能是数字',1,'regex',3),
    );

    // 自动完成
    protected $_auto = array (
        // 时间转成时间戳
        array('addtime','time',1,'function'), 
        // 管理员aid
        array('user_uid','_uid',1,'callback'),
    );

    // 用户uid
	public function _uid()
	{
		return session('user_id');
	}

	public function get_all($map,$order,$sort,$currentPage,$listRows)
	{
		$data = D('AdView')->where($map)->order($order.' '.$sort)->page($currentPage.','.$listRows)->select();
		return $data;
	}



	public function _before_insert(&$data,$option)
	{
		$pic = $this->alter_pic();
		if($pic)
			$data['pic'] = $pic;
		$picEn = $this->alter_pic_en();
		if($picEn)
			$data['pic_en'] = $picEn;
	}


	public function _before_update(&$data,$option)
	{
		$aid = I('post.aid');
		$pic = $this->alter_pic($aid);
		if($pic)
			$data['pic'] = $pic;
		$picEn = $this->alter_pic_en();
		if($picEn)
			$data['pic_en'] = $picEn;
	}


	/**
	 * [alter_pic 上传图片]
	 * @return [type] [description]
	 */
	public function alter_pic($aid=null)
	{

		
		if($aid&&!empty($_FILES['pic']['name']))
		{
			$field = $this->where(array('aid'=>$aid))->find();
			
			is_file($field['pic']) and unlink($field['pic']);
		}

		if(!empty($_FILES['pic']['name']))
		{
			// 上传类
			$upload = new Upload();             // 实例化上传类
			$upload->maxSize  = 314572800 ;     // 设置附件上传大小
			$upload->exts  = explode('|', C('cfg_image'));// 设置附件上传类型
			$upload->autoSub =false;            //不要自动创建子目录
			$upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
			$upload->savePath = 'image/'.date('Y').'/'.date('m').'/'.date('d').'/';
            // 执行上传
            if($info=$upload->uploadOne($_FILES['pic']))
                return  $upload->rootPath.$info['savepath'].$info['savename'];
            else
            {
            	$this->error=$upload->getErrorMsg();
            	return false;
            }

		}
		else
			return '';
	}

	/**
	 * [alter_pic_en 上传图片]
	 * @return [type] [description]
	 */
	public function alter_pic_en($aid=null)
	{

		
		if($aid&&!empty($_FILES['pic_en']['name']))
		{
			$field = $this->where(array('aid'=>$aid))->find();
			
			is_file($field['pic_en']) and unlink($field['pic_en']);
		}

		if(!empty($_FILES['pic_en']['name']))
		{
			// 上传类
			$upload = new Upload();             // 实例化上传类
			$upload->maxSize  = 314572800 ;     // 设置附件上传大小
			$upload->exts  = explode('|', C('cfg_image'));// 设置附件上传类型
			$upload->autoSub =false;            //不要自动创建子目录
			$upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
			$upload->savePath = 'image/'.date('Y').'/'.date('m').'/'.date('d').'/';
            // 执行上传
            if($info=$upload->uploadOne($_FILES['pic_en']))
                return  $upload->rootPath.$info['savepath'].$info['savename'];
            else
            {
            	$this->error=$upload->getErrorMsg();
            	return false;
            }

		}
		else
			return '';
	}

	/**
	 * [get_one 获取单条数据]
	 * @param  [type] $aid [description]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_one($aid)
	{
		return $this->find($aid);
	}

	/**
	 * [del 删除]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function del($aid)
	{
		$pic = $this->where(array('aid'=>$aid))->getField('pic');
		is_file($pic) && unlink($pic);
		$this->delete($aid);
		return true;
	}


	
	/**
	 * [update_sort 更新排序]
	 * @param  [type] $aid   [description]
	 * @param  [type] $sort [description]
	 * @return [type]       [description]
	 */
	public function update_sort($aid,$sort)
	{

		foreach($aid as $k=>$v)
		{
			$this->save(array('sort'=>$sort[$k],'aid'=>$v));
		}
		return true;
	}


	/**
	 * [update_check_state 更新审核状态]
	 * @param  [type] $aid    [description]
	 * @param  [type] $status [description]
	 * @return [type]         [description]
	 */
    public function update_check_state($aid,$status)
    {
    	foreach($aid as $k=>$v)
		{
			$this->save(array('verifystate'=>$status,'aid'=>$v));
		}
		return true;
		
    }



}