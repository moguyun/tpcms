<?php
/** [******]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 18:25:27
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 15:40:53
 */
namespace Admin\Logic;
use Think\Model;
use Think\Upload;
class ConfigLogic extends Model{

	// 自动验证
	/* array(验证字段,验证规则,错误提示,[验证条件,附加规则,验证时间])
	*
	*  验证条件
	*  Model::EXISTS_VALIDATE 或者0 存在字段就验证 （默认）
	*  Model::MUST_VALIDATE 或者1 必须验证
	*  Model::VALUE_VALIDATE或者2 值不为空的时候验证
	*
	*  验证时间
	*  Model:: MODEL_INSERT 或者1新增数据时候验证
	*  Model:: MODEL_UPDATE 或者2编辑数据时候验证
	*  Model:: MODEL_BOTH 或者3 全部情况下验证（默认）
	* */
 	protected $_validate = array(

 		array('title','require','标题不能为空',1),
 		array('title','check_title','标题不能重复',1,'callback'),
 		array('code','require','变量不能为空',1),
 		array('code','/^cfg_([a-z_])+/i','变量必须以cfg_开头,且只能包含是英文或者下划线',1),
 		array('code','check_code','变量不能重复',1,'callback'),
 		array('sort','number','排序只能是数字',1),

 	);
 	/**
 	 * [check_title 标题不能重复]
 	 * @param  [type] $con [description]
 	 * @return [type]      [description]
 	 */
 	protected function check_title($con)
 	{
 		if($this->where(array('title'=>$con))->find())
 			return false;
 		return true;
 	}
 	/**
 	 * [check_code 变量不能重复]
 	 * @param  [type] $con [description]
 	 * @return [type]      [description]
 	 */
 	protected function check_code($con)
 	{
 		if($this->where(array('code'=>$con))->find())
 			return false;
 		return true;
 	}



	/**
	 * [get_all 读取所有的信息的表单]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		$data = $this->order('sort asc,id asc')->select();
		if(!$data) return $data;

		// 组合表单
		foreach($data as $v)
		{
			switch ($v['config_type']) 
			{
				case 1:
					$function = 'img';
					break;
				case 2:
					$function = 'input';
					break;
				case 3:
					$function = 'textarea';
					break;
				case 4:
					$function = 'file';
					break;
				case 5:
					$function = 'radio';
					break;
			}
			
			$html=$this->$function($v,$v['body']);

			$form[] = array(
				'title'=>$v['title'],
				'form'=>$html,
				'group'=>$v['group'],
				'code'=>$v['code'],
				'sort'=>$v['sort'],
			);
		}

		$result =array();
		// 分组
		foreach($form as  $v)
		{
			$result[$v['group']][]=$v;
		}
		return $result;
	}
	/**
	 * [img 上传]
	 * @param  [type] $field [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function img($field,$value)
	{

		$html = "<input type='file' name='{$field['code']}' >";
		if($value)
		{
			// 图片路径
			$src = __ROOT__."/".$value;
			$html .= "<br/><img src ='{$src}' width='100'/>";
		}
		return $html;
	}
	/**
	 * [file 上传]
	 * @param  [type] $field [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function file($field,$value)
	{
		$html = "<input type='file' name='{$field['code']}' class='file-input'>";
		if($value)
		{
			// 组合下载地址
			$value =str_replace('/', '|', $value);
			$url = U('down')."&name=".$value;
			$html .= "<br/><a href='{$url}' >点击下载</a>";
		}
		return $html;
	}
	/**
	 * [input 单行文本]
	 * @param  [type] $field [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function input($field,$value)
	{
		$html = "<input type='text'  class='hd-w300' name='{$field['code']}' value='{$value}' />";
		return $html;

	}
	/**
	 * [textarea 多行文本]
	 * @param  [type] $field [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function textarea($field,$value)
	{
		$html ="<textarea class='hd-w500' style='overflow: hidden; word-wrap: break-word; resize: horizontal; height: 100px;' name='{$field['code']}'>{$value}</textarea>";
		return $html;

	}

	/**
	 * [radio 多行文本]
	 * @param  [type] $field [description]
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function radio($field,$value)
	{
		$select=$noselect='';

		if($value ==1)
			$select= 'checked ="checked"';
		else
			$noselect=  'checked ="checked"';

		$html ="<label><input type='radio' name='{$field['code']}' value='1' {$select}> 是</label>
				<label><input type='radio' name='{$field['code']}' value='0' {$noselect}> 不是</label>";
		return $html;

	}



	/**
	 * [save_config 系统设置]
	 * @return [type] [description]
	 */
	public function save_config()
	{
		$data = $_POST;

		// header("Content-type:text/html;charset=utf-8");
		$config = $this->order('sort asc,id asc')->select();
		
		foreach($config as $v)
		{
			$result['sort'] = $data['sort'][$v['code']];
			$result['body'] = '';
			// 没有上框
			if(isset($data[$v['code']]))
				// 组合数组
				$result['body'] = $data[$v['code']];
				
			// 上传图片
			if($v['config_type']==1 || $v['config_type']==4)
			{
				// 判断是否有图片上传
				if($_FILES[$v['code']]['name'])
				{
					// 上传类
					$upload = new Upload();             // 实例化上传类
					$upload->maxSize  = 314572800 ;     // 设置附件上传大小
					$upload->exts  = explode('|', C('cfg_file'));// 设置附件上传类型
					$upload->autoSub =false;            //不要自动创建子目录
					$upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
					if($v['config_type']==1)
						$upload->savePath = 'image/'.date('Y').'/'.date('m').'/'.date('d').'/';
					else
						$upload->savePath = 'file/'.date('Y').'/'.date('m').'/'.date('d').'/';
					$info=$upload->uploadOne($_FILES[$v['code']]);

					// 上传错误
					if(!$info)
					{
						$this->error = $upload->getErrorMsg();
						return false;
					}
					// 保存信息
					$img = $upload->rootPath.$info['savepath'].$info['savename'];
					$result['body'] = $img;
					// 删除旧信息
					is_file($v['body']) && unlink($v['body']);
				}
				else
				 	unset($result['body']);
			}
			$this->where(array('id'=>$v['id']))->save($result);
		}
		return true;
	}
	/**
	 * [write_config 写文件]
	 * @return [type] [description]
	 */
	public function write_config()
 	{
 		// 读取所有的信息
 		$config = $this->order('sort asc,id asc')->select();
 		if(!$config) return ;
 		$data = array();
 		// 循环配置
 		foreach ($config as $v) 
 		{
 			$data[$v['code']] = $v['body'];
 		}
 		// 写文件
 		return file_put_contents("Data/Config/config.inc.php", "<?php \nreturn " . var_export($data, true) . ";\n?>");
 	}
}