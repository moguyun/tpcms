<?php
/** [配送方式表逻辑层模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-02-06 10:08:16
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class shippingLogic extends Model{



	/**
	 * [$_validate 自动验证]
	 * @var array
	 */
	protected $_validate = array(
		array('shipping_name','require','请输入配送方式名称',1),
		array('shipping_desc','require','请输入配送方式描述',1),
	);

	


	/**
	 * [get_all 所有配送方式]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		$data = $this->select();
		foreach($data  as $k=>$v)
		{
			$data[$k]['disabled'] = $v['is_disabled']?'禁用':'启用';
		}
		return $data;
	}
	/**
	 * [get_one 读取配送方式]
	 * @return [type] [description]
	 */
	public function get_one($id)
	{
		return $this->find($id);
	}



	
	
	/**
	 * [del 删除]
	 * @return [type] [description]
	 */
	public function del($id)
	{
		$area = D('ShippingArea')->where(array('shipping_shipping_id'=>$id))->getField('area_id',true);
		D('ShippingArea')->where(array('shipping_shipping_id'=>$id))->delete();
		D('RelationShippingAreaRegion')->where(array('shipping_area_area_id'=>array('in',$area)))->delete();
		$this->delete($id);

		return true;
	}
}