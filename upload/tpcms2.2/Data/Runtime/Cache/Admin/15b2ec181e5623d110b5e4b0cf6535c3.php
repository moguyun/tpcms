<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/test/tpcms/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/test/tpcms/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/test/tpcms/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/test/tpcms/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/test/tpcms/index.php/Admin'; //当前模块
CONTROLLER='/test/tpcms/index.php/Admin/Category'; //当前控制器)
ACTION='/test/tpcms/index.php/Admin/Category/index';//当前方法(方法)
ROOT='/test/tpcms'; //当前项目根路径
PUBLIC= '/test/tpcms/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/test/tpcms/Data/Public/org/wind.js"></script>
<script src="/test/tpcms/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
		<ul class="cc">
			<li class="current"><a href="javascrip:;">栏目列表</a></li>
			<li ><a href="<?php echo U('Category/add');?>">添加栏目</a></li>
			<li ><a href="<?php echo U('Category/update_cache');?>">更新栏目缓存</a></li>
		</ul>
	</div>
  <form name="myform" action="<?php echo U('Category/sort');?>" method="post" class="J_ajaxForm">
  <div class="table_list">
    <table width="100%">
        <colgroup>
	        <col width="50">
	        <col width="55">
	      
	        <col>
	        <col width="80">
	        <col width="100">
	        <col width="50" >
	        <col width="300">
        </colgroup>
        <thead>
          <tr>
          	<td>Cid</td>
            <td align='center'>排序</td>
            <td>栏目名称</td>
            <td align='center'>栏目类型</td>
            <td>所属模型</td>
            <td align='center'>访问</td>
            <td align='center'>管理操作</td>
          </tr>
        </thead>
        <?php if($category): if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr cid='<?php echo ($value["cid"]); ?>' <?php if($value['pid'] == 0): ?>class="top"<?php endif; ?> >
        	<td> <?php echo ($value["cid"]); ?></td>
    			 <td align='center'>
    				<input name='sort[<?php echo ($value["cid"]); ?>]' type='text' size='3' value='<?php echo ($value["sort"]); ?>' class='input'>
    			</td>
  			
			   <td >
				
				<?php if($value['pid'] == 0 && Third\Data::hasChild(S('category'),$value['cid'])): ?><img src="/test/tpcms/Data/Public/admin/images/contract.gif" action="2" class="explodeCategory hand"/><?php endif; ?>
				<?php if($value['pid'] == 0): ?><strong><?php echo ($value["_name"]); ?></strong>
				<?php else: ?>
					<?php echo ($value["_name"]); endif; ?>

			</td>
			<td  align='center'><?php echo ($value["type"]); ?></td>
			<td><?php echo ($value["model"]); ?></td>
			<td align='center'><a href='<?php if($value['cat_type'] == 4): echo U('/'.strtolower($value['remark'])."_v_".$value['cid']); else: echo U('/'.strtolower($value['remark'])."_l_".$value['cid']); endif; ?>' target='_blank'>访问</a></td>
			<td align='center' >
				<a href="<?php echo U('Category/add',array('pid'=>$value['cid']));?>">添加子栏目</a> | 
				<a href="<?php echo U('Category/edit',array('cid'=>$value['cid']));?>">修改</a> | 
				<a class="J_ajax_del" href="<?php echo U('Category/del',array('cid'=>$value['cid']));?>">删除</a>  
			</td>
		</tr><?php endforeach; endif; else: echo "" ;endif; ?>
    <?php else: ?>
        <tr>
          <td colspan="7">没有找到任何符合条件记录</td>
        </tr><?php endif; ?>
	</table>
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">排序</button>
      </div>
    </div>
  </div>

</form>
</div>
<script type="text/javascript" src="/test/tpcms/Data/Public/admin/js/mod.common.js"></script>
<script>
var PUBLIC = '/test/tpcms/Data/Public/admin';
</script>
<script type="text/javascript" src="/test/tpcms/Data/Public/admin/js/mod.category.js"></script>
</body>
</html>